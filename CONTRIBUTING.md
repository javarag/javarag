# Contributing to JavaRAG
JavaRAG is an open source project, released under the MIT license.
Contributions are very welcome and should ideally follow this process:

1. **Issue.** Create or comment on an issue in the issue tracker with the proposed change.

2. **Pull request.** Create a pull request with your changes.

3. **Review.** If the pull request is good it will be merged, otherwise go to step 2.

Thank you for your contribution!