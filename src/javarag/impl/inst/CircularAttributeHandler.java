package javarag.impl.inst;

import java.util.HashMap;
import java.util.Map;

public class CircularAttributeHandler implements AttributeHandler {
	private final AttributeHandler bottom;
	private final AttributeHandler attribute;
	private final Map<Request, Object> cache;

	public CircularAttributeHandler(AttributeHandler bottom, AttributeHandler attribute) {
		this.bottom = bottom;
		this.attribute = attribute;
		this.cache = new HashMap<>();
	}

	@Override
	public String getName() {
		return attribute.getName();
	}

	@Override
	public Object evaluate(Context context, Request request) {
		if (cache.containsKey(request)) {
			return cache.get(request);
		}
		FixPointContext fixPointContext = context.getFixPointContext();
		if (!fixPointContext.containsValue(request)) {
			fixPointContext.putValue(request, bottom.evaluate(context, request));
		}
		if (!fixPointContext.isInCycle()) {
			fixPointContext.enterCycle();
			Object result;
			do {
				fixPointContext.visit(request);
				fixPointContext.setNotChanged();
				result = attribute.evaluate(context, request);
				fixPointContext.putValue(request, result);
				fixPointContext.incrementIteration();
			} while (fixPointContext.isChanged());
			cache.put(request, result);
			cache.putAll(fixPointContext.clearIntermediateValues());
			fixPointContext.exitCycle();
			return result;
		} else if (!fixPointContext.isVisited(request)) {
			fixPointContext.visit(request);
			Object result = attribute.evaluate(context, request);
			fixPointContext.putValue(request, result);
			context.getCachingContext().setIsIntermediate(true);
			return result;
		} else {
			context.getCachingContext().setIsIntermediate(true);
			return fixPointContext.getValue(request);
		}
	}

}
